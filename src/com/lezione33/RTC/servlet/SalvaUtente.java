package com.lezione33.RTC.servlet;

import java.io.IOException;
import java.io.PrintWriter;
import java.sql.SQLException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.google.gson.Gson;
import com.lezione33.RTC.model.Utente;
import com.lezione33.RTC.services.UtenteDAO;
import com.lezione33.RTC.utility.ResponsoOperazione;

@WebServlet("/salvaUtente")
public class SalvaUtente extends HttpServlet {


    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        String varUser = request.getParameter("input_user");
        String varPassword = request.getParameter("input_password");

        Utente temp = new Utente();
        temp.setUserName(varUser);
        temp.setPassword(varPassword);

        UtenteDAO user_dao = new UtenteDAO();
        String strErrore = "";
        try {
            user_dao.insert(temp);
        } catch (SQLException e) {
            strErrore = e.getMessage();
        }

        PrintWriter out = response.getWriter();
        Gson jsonizzatore = new Gson();

        if(temp.getUtenteId() != null) {
//          out.print(jsonizzatore.toJson(new ResponsoOperazione("OK")));

            ResponsoOperazione res = new ResponsoOperazione("OK", "");
            out.print(jsonizzatore.toJson(res));
        }
        else {
            ResponsoOperazione res = new ResponsoOperazione("ERRORE", strErrore);
            out.print(jsonizzatore.toJson(res));
        }
    }


}