package com.lezione33.RTC.services;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;

import com.lezione33.RTC.connessioni.ConnettoreDB;
import com.lezione33.RTC.model.Utente;

public class UtenteDAO implements DAO<Utente> {


    @Override
    public ArrayList<Utente> getAll() throws SQLException {
        ArrayList<Utente> elenco = new ArrayList<Utente>();

        Connection conn = ConnettoreDB.getIstanza().getConnessione();

        String query = "SELECT id, ruolo, nick, pass FROM utenti";
        PreparedStatement ps = (PreparedStatement) conn.prepareStatement(query);

        ResultSet risultato = ps.executeQuery();
        while(risultato.next()){
            Utente temp = new Utente();
            temp.setUtenteId(risultato.getInt(1));
            temp.setRuolo(risultato.getString(4));
            temp.setUserName(risultato.getString(2));
            temp.setPassword(risultato.getString(3));

            elenco.add(temp);
        }

        return elenco;
    }

    @Override
    public void insert(Utente t) throws SQLException {

        Connection conn = ConnettoreDB.getIstanza().getConnessione();

        String query = "INSERT INTO utenti (ruolo, nick, pass) VALUE (?,?,?)";
        PreparedStatement ps = (PreparedStatement) conn.prepareStatement(query, Statement.RETURN_GENERATED_KEYS);
        ps.setString(1, "simple");
        ps.setString(2, t.getUserName());
        ps.setString(3, t.getPassword());


        ps.executeUpdate();
        ResultSet risultato = ps.getGeneratedKeys();
        risultato.next();

        t.setUtenteId(risultato.getInt(1));
    }


    @Override
    public boolean delete(Utente t) throws SQLException {

        Connection conn = ConnettoreDB.getIstanza().getConnessione();
        String query = "DELETE FROM utenti WHERE id = ?";
        PreparedStatement ps = (PreparedStatement) conn.prepareStatement(query);
        ps.setInt(1, t.getUtenteId());

        int affRows = ps.executeUpdate();
        if(affRows > 0)
            return true;

        return false;
    }

    @Override
    public Utente update(Utente t) throws SQLException {

        Connection conn = ConnettoreDB.getIstanza().getConnessione();
        String query = "UPDATE utenti SET "
                + "ruolo = ?, "
                + "nick = ?, "
                + "pass = ? "
                + "WHERE id = ?";
        PreparedStatement ps = (PreparedStatement) conn.prepareStatement(query);
        ps.setString(1, t.getRuolo());
        ps.setString(2, t.getUserName());
        ps.setString(3, t.getPassword());
        ps.setInt(4, t.getUtenteId());

        int affRows = ps.executeUpdate();
        if(affRows > 0)
            return getById(t.getUtenteId());

        return null;

    }

    @Override
    public Utente getByUser(String user) throws SQLException {

        Connection conn = ConnettoreDB.getIstanza().getConnessione();

        String query = "SELECT id, ruolo, nick, pass FROM utenti WHERE nick = ?";
        PreparedStatement ps = (PreparedStatement) conn.prepareStatement(query);
        ps.setString(1, user);

        ResultSet risultato = ps.executeQuery();
        risultato.next();
        Utente temp = new Utente();
        temp.setUtenteId(risultato.getInt(1));
        temp.setRuolo(risultato.getString(2));
        temp.setUserName(risultato.getString(3));
        temp.setPassword(risultato.getString(4));

        return temp;
    }

	@Override
	public Utente getById(int id) throws SQLException {
		Connection conn = ConnettoreDB.getIstanza().getConnessione();

        String query = "SELECT id, ruolo, nick, pass FROM utenti WHERE id = ?";
        PreparedStatement ps = (PreparedStatement) conn.prepareStatement(query);
        ps.setInt(1, id);

        ResultSet risultato = ps.executeQuery();
        risultato.next();
        Utente temp = new Utente();
        temp.setUtenteId(risultato.getInt(1));
        temp.setRuolo(risultato.getString(2));
        temp.setUserName(risultato.getString(3));
        temp.setPassword(risultato.getString(4));

        return temp;
	}


}
