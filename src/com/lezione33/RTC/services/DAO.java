package com.lezione33.RTC.services;

import java.sql.SQLException;
import java.util.ArrayList;

public interface DAO <T>{
	
	T getById(int id) throws SQLException;

	T getByUser(String user) throws SQLException;	//Lo uso per verificare il login

    ArrayList<T> getAll() throws SQLException;

    void insert(T t) throws SQLException;

    boolean delete(T t) throws SQLException;

    T update(T t) throws SQLException;
    
}
