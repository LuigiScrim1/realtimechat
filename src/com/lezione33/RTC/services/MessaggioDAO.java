package com.lezione33.RTC.services;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.sql.Timestamp;
import java.time.LocalDateTime;
import java.util.ArrayList;

import com.lezione33.RTC.connessioni.ConnettoreDB;
import com.lezione33.RTC.model.Messaggio;
import com.lezione33.RTC.model.Utente;
import com.mysql.jdbc.PreparedStatement;
import com.sun.net.httpserver.Authenticator.Result;

public class MessaggioDAO implements DAO<Messaggio> {

	@Override
	public ArrayList<Messaggio> getAll() throws SQLException {
		ArrayList<Messaggio> elenco = new ArrayList<Messaggio>();
		Connection conn = ConnettoreDB.getIstanza().getConnessione();
		String query = "SELECT id, contenuto, dataOra, rifUser FROM messaggi ORDER BY dataOra ASC";
		PreparedStatement ps = (PreparedStatement) conn.prepareStatement(query);
		
		ResultSet risultato = ps.executeQuery();
		while(risultato.next()) {
			Messaggio msg = new Messaggio();
			msg.setMessaggioId(risultato.getInt(1));
			msg.setContenuto(risultato.getString(2));
			msg.setDataOra(risultato.getTimestamp(3).toLocalDateTime());
			msg.setRif(risultato.getInt(4));
			elenco.add(msg);
		}
		return elenco;
	}

	@Override
	public void insert(Messaggio t) throws SQLException {
		Connection conn = ConnettoreDB.getIstanza().getConnessione();
		String query = "INSERT INTO messaggi (contenuto, dataOra, rifUser) values (?,?,?)";
		PreparedStatement ps = (PreparedStatement) conn.prepareStatement(query, Statement.RETURN_GENERATED_KEYS);
		ps.setString(1, t.getContenuto());
		ps.setTimestamp(2, Timestamp.valueOf(t.getDataOra()));
		ps.setInt(3, t.getRif());
		
		ps.executeUpdate();
		ResultSet risultato = ps.getGeneratedKeys();
		risultato.next();
		t.setMessaggioId(risultato.getInt(1));
	}

	@Override
	public boolean delete(Messaggio t) throws SQLException {
		Connection conn = ConnettoreDB.getIstanza().getConnessione();
		String query = "DELETE FROM messaggi where id = ?";
		PreparedStatement ps= (PreparedStatement) conn.prepareStatement(query);;
		ps.setInt(1, t.getMessaggioId());
		
		int affRows = 0;
		if(affRows>0)
			return true;
		
		return false;
	}

	@Override
	public Messaggio update(Messaggio t) throws SQLException {
		Connection conn = ConnettoreDB.getIstanza().getConnessione();
   		String query = "UPDATE messaggi SET contenuto = ?, dataOra = ?, rifUser = ? WHERE id = ? ";
       	PreparedStatement ps = (PreparedStatement) conn.prepareStatement(query);
       	ps.setString(1, t.getContenuto());
       	ps.setTimestamp(2, Timestamp.valueOf(t.getDataOra()));
       	ps.setInt(3, t.getRif());
       	ps.setInt(4, t.getMessaggioId());
       	
       	int affRows = ps.executeUpdate();
       	if(affRows > 0)
       		return getById(t.getMessaggioId());
		return null;
	}

	@Override
	public Messaggio getByUser(String user) throws SQLException {
		return null;
	}

	@Override
	public Messaggio getById(int id) throws SQLException {
		Connection conn = ConnettoreDB.getIstanza().getConnessione();

        String query = "SELECT id, contenuto, dataOra, rifUser FROM messaggi WHERE id = ?";
        PreparedStatement ps = (PreparedStatement) conn.prepareStatement(query);
        ps.setInt(1, id);

        ResultSet risultato = ps.executeQuery();
        risultato.next();
        Messaggio temp = new Messaggio();
        temp.setMessaggioId(risultato.getInt(1));
        temp.setContenuto(risultato.getString(2));
        temp.setDataOra(risultato.getTimestamp(3).toLocalDateTime());
        temp.setRif(risultato.getInt(4));

        return temp;
	}
	

}
