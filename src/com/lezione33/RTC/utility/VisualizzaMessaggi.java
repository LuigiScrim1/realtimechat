package com.lezione33.RTC.utility;

import java.io.IOException;
import java.io.PrintWriter;
import java.sql.SQLException;
import java.util.ArrayList;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.google.gson.Gson;
import com.lezione33.RTC.model.Messaggio;
import com.lezione33.RTC.services.MessaggioDAO;


@WebServlet("/visualizzaMessaggi")
public class VisualizzaMessaggi extends HttpServlet {

    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

        PrintWriter out = response.getWriter();
        response.setContentType("application/json");
        response.setCharacterEncoding("UTF-8");

        MessaggioDAO messDao = new MessaggioDAO();

        try {

        ArrayList<Messaggio> elencoMessaggi = messDao.getAll();
        String risultatoJson = new Gson().toJson(elencoMessaggi);
        out.print(risultatoJson);

        } catch (SQLException e) {

            out.print(new Gson().toJson(new ResponsoOperazione("ERRORE", e.getMessage()))); //Molto compatta
            System.out.println(e.getMessage());

        }

    }

}
