package com.lezione33.RTC.model;

import java.util.ArrayList;

public class Utente {

    private Integer utenteId;
    private String userName;
    private String password;
    private String ruolo;
    private ArrayList<Messaggio> elencoMessaggi;

    public Utente() {

    }

    public Utente(Integer utenteId, String userName, String password, String ruolo,
            ArrayList<Messaggio> elencoMessaggi) {
        super();
        this.utenteId = utenteId;
        this.userName = userName;
        this.password = password;
        this.ruolo = ruolo;
        this.elencoMessaggi = elencoMessaggi;
    }

    public Integer getUtenteId() {
        return utenteId;
    }

    public void setUtenteId(Integer utenteId) {
        this.utenteId = utenteId;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getRuolo() {
        return ruolo;
    }

    public void setRuolo(String ruolo) {
        this.ruolo = ruolo;
    }

    public ArrayList<Messaggio> getElencoMessaggi() {
        return elencoMessaggi;
    }

    public void setElencoMessaggi(ArrayList<Messaggio> elencoMessaggi) {
        this.elencoMessaggi = elencoMessaggi;
    }

}