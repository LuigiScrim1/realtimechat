package com.lezione33.RTC.model;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

public class Messaggio {

    private Integer messaggioId;
    private String contenuto;
    private LocalDateTime dataOra;
    private Integer rif;

    public Integer getRif() {
		return rif;
	}

	public void setRif(Integer rif) {
		this.rif = rif;
	}

	public Messaggio() {

    }

    public Messaggio(Integer messaggioId, String contenuto, LocalDateTime dataOra, Integer rif) {
        super();
        this.messaggioId = messaggioId;
        this.contenuto = contenuto;
        this.dataOra = dataOra;
        this.rif = rif;
    }

    public Integer getMessaggioId() {
        return messaggioId;
    }

    public void setMessaggioId(Integer messaggioId) {
        this.messaggioId = messaggioId;
    }

    public String getContenuto() {
        return contenuto;
    }

    public void setContenuto(String contenuto) {
        this.contenuto = contenuto;
    }

    public LocalDateTime getDataOra() {
        return dataOra;
    }

    public void setDataOra() {
        LocalDateTime giornoOraCorrente = LocalDateTime.now();
//        DateTimeFormatter formattazione_ita = DateTimeFormatter.ofPattern("yyyy-mm-dd HH:mm:ss");
//        String data_formato_ita = giornoOraCorrente.format(formattazione_ita);
//        LocalDateTime dataCorretta = LocalDateTime.parse(data_formato_ita);
        this.dataOra = giornoOraCorrente;
    }

	public void setDataOra(LocalDateTime localDateTime) {
		this.dataOra = localDateTime;
	}



}