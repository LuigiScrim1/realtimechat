function stampaMessaggi(arr_messaggio){
    let contenuto='';

    for(let i=0; i<arr_messaggio.length; i++){
			contenuto += cardRisultato(arr_messaggio[i]);
			window.setInterval(function(){
				$("#mia-card").html(contenuto);
			},1000);
			
    }
	
	
    
}

function cardRisultato(obj_messaggio){

   		var riga = '<div class="card" style="width: 18rem;">';
        riga+='<div class="card-body">';
		riga+='<h5 class="card-title">'+ obj_messaggio.rif + '</h5>';
        riga+='<h5 class="card-subtitle">' + obj_messaggio.dataOra.date.day + "/" +
                    obj_messaggio.dataOra.date.month + "/"    +
                    obj_messaggio.dataOra.date.year + "-" +
                    obj_messaggio.dataOra.time.hour + ":" +
                    obj_messaggio.dataOra.time.minute + ":" +
                    obj_messaggio.dataOra.time.second +
                '<\h6>';
        riga+= '<p class="card-text">' + obj_messaggio.contenuto + '<\p>';
        riga+='</div>';
        riga+='</div>';

    return riga;

}


$(document).ready(
	
	
	function() {
		
		let utente=null;
		let orario = null;
		$("#reload").click(
			function(){
				$.ajax(
			{
				url:"http://localhost:8080/RealtTimeChat/visualizzaMessaggi",
				method: "POST",
				success: function(risultato){
					stampaMessaggi(risultato);
					console.log("AGGIORNATO");
				},
				error: function(errore){
					console.log(errore);
				}
			}
		);
		}
	);
		
		
		$("#btn-logon").click(
			function(){
				let user=$("#username").val();
				let pswd = $("#password").val();
				
				$.ajax(
					{
						url:"http://localhost:8080/RealtTimeChat/salvaUtente",
						method: "POST",
						data:{
							input_user : user,
							input_password : pswd
						},
						success: function(risultato){
							let risJson=JSON.parse(risultato);
							alert("Registrazione effettuata con successo");
						},
						error: function(errore){
							let eJson = JSON.parse(errore);
						}	
					}
				);
			}
		);
	}
);